import torch
import torch.nn as nn
import torch.nn.functional as F


class Attention(nn.Module):
    def __init__(self, scaled, is_training=True):
        super().__init__()

        self.scaled = scaled
        self.is_training = is_training

    def forward(self, q, k, v, dropout=0.1, pad_mask=None, sub_mask=False, self_mask=False):
        attention = torch.matmul(q / self.scaled, k.transpose(2, 3))

        if pad_mask is not None:
            attention.masked_fill_(pad_mask, -1e9)

        if sub_mask:
            sub_mask = torch.tril(torch.ones(attention.size(2), attention.size(2), device=attention.device))
            attention.masked_fill_(sub_mask == 0, -1e9)
            del sub_mask

        if self_mask:
            self_mask = torch.eye(attention.size(2), device=attention.device)
            attention.masked_fill_(self_mask == 1, -5e4)
            del self_mask

        attention = F.softmax(attention, dim=-1)
        attention = F.dropout(attention, p=dropout, training=self.is_training)
        output = torch.matmul(attention, v)

        return output
