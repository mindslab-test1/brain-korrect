import random
import torch
import sentencepiece as spm
import re


class Change:
    def __init__(self, itog, seq_len, tok_len):
        self.itog = itog
        self.tok_len = tok_len
        self.seq_len = seq_len

        self.gtoi = {v: i for i, v in enumerate(itog)}
        self.buh = {
            'ㄱㅋㄲ': 'ㅇ',
            'ㅂㅍ': 'ㅁ',
            'ㄷㅅㅆㅈㅊㅌ': 'ㄴ'
        }
        self.gguh = {
            'ㄷ': 'ㅈ',
            'ㅌ': 'ㅊ'
        }

        self.gkok = {
            'ㄱ': 'ㅋ',
            'ㄷ': 'ㅌ',
            'ㅂ': 'ㅍ',
            'ㅈ': 'ㅊ'
        }

        self.ejung = {
            'ㄳ': 'ㄱㅅ',
            'ㄵ': 'ㄴㅈ',
            'ㄶ': 'ㄴㅎ',
            'ㄺ': 'ㄹㄱ',
            'ㄻ': 'ㄹㅁ',
            'ㄼ': 'ㄹㅂ',
            'ㄽ': 'ㄹㅅ',
            'ㄾ': 'ㄹㅌ',
            'ㅀ': 'ㄹㅎ',
            'ㅄ': 'ㅂㅅ'
        }

        self.yamin = {
            '대': '머',
            '귀': '커',
            '파': '과',
            '피': '끠',
            '비': '네',
            '며': '댸',
            '거': '지',
            '겨': '저',
            '교': '꼬',
            '유': '윾',
            '우': '윽',
            '웃': '읏',
            '을': '울',
            '왕': '앟',
            '왱': '앻',
            '욍': '잏',
            '왓': '앛',
            '왯': '앷',
            '욋': '잋',
            'ㅇ': 'o',
            'ㄱ': '7',
            'ㄹ': '2',
            '디': 'ㅁ',
            '구': 'ㅋ',
            '너': 'ㅂ',
            '빅': '븨',
            '근': 'ㄹ',
            '긘': '리'
        }

        self.yamin.update({v: k for k, v in self.yamin.items()})

        self.cjz = [list("ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ"),
                    list("ㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ"),
                    [chr(12644)] + list("ㄱㄲㄳㄴㄵㄶㄷㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅄㅅㅆㅇㅈㅊㅋㅌㅍㅎ")]

        wsh = 'ㄱㄲㄳㄴㄵㄶㄷㄸㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅃㅄㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ'
        zhh = 'ᄀᄁᆪᄂᆬᆭᄃᄄᄅᆰᆱᆲᆳᆴᆵᄚᄆᄇᄈᄡᄉᄊᄋᄌᄍᄎᄏᄐᄑ하ᅢᅣᅤᅥᅦᅧᅨᅩᅪᅫᅬᅭᅮᅯᅰᅱᅲᅳᅴᅵ'

        self.johapsiba = {k: v for k, v in zip(zhh, wsh)}

        self.bunhe = {
            'ㄳ': 'ㄱㅅ',
            'ㄵ': 'ㄴㅈ',
            'ㄶ': 'ㄴㅎ',
            'ㄺ': 'ㄹㄱ',
            'ㄻ': 'ㄹㅁ',
            'ㄼ': 'ㄹㅂ',
            'ㄽ': 'ㄹㅅ',
            'ㄾ': 'ㄹㅌ',
            'ㄿ': 'ㄹㅍ',
            'ㅀ': 'ㄹㅎ',
            'ㅄ': 'ㅂㅅ',
            'ㅘ': 'ㅗㅏ',
            'ㅙ': 'ㅗㅐ',
            'ㅚ': 'ㅗㅣ',
            'ㅝ': 'ㅜㅓ',
            'ㅞ': 'ㅜㅔ',
            'ㅟ': 'ㅜㅣ',
            'ㅢ': 'ㅡㅣ',
            chr(12644): ''
        }

        self.hebun = {k: v for v, k in self.bunhe.items()}

        qwertyeng = "QqWwEeRrTtyuiOoPpasdfghjklzxcvbnm"
        qwertykor = "ㅃㅂㅉㅈㄸㄷㄲㄱㅆㅅㅛㅕㅑㅒㅐㅖㅔㅁㄴㅇㄹㅎㅗㅓㅏㅣㅋㅌㅊㅍㅠㅜㅡ"
        self.hanyoung = {k: v for k, v in zip(qwertykor, qwertyeng)}

        self.phocjz = [[list('ㄲㅋ'),
                        list('ㄱㅋ'),
                        list('ㄴ'),
                        list('ㄸㅌ'),
                        list('ㄷㅌ'),
                        list('ㄹ'),
                        list('ㅁ'),
                        list('ㅃㅍ'),
                        list('ㅂㅍ'),
                        list('ㅆ'),
                        list('ㅅ'),
                        list('ㅇ'),
                        list('ㅉㅊ'),
                        list('ㅈㅊ'),
                        list('ㅈㅉ'),
                        list('ㄱㄲ'),
                        list('ㄷㄸ'),
                        list('ㅂㅃ'),
                        list('ㅎ')
                        ],
                       [list('ㅑㅘ'),
                        list('ㅒㅔㅖㅙㅚㅞ'),
                        list('ㅏㅘ'),
                        list('ㅐㅔㅖㅙㅚㅞ'),
                        list('ㅕㅗㅛㅝ'),
                        list('ㅐㅒㅖㅙㅚㅞ'),
                        list('ㅓㅗㅛㅝ'),
                        list('ㅐㅒㅔㅙㅚㅞ'),
                        list('ㅓㅕㅛㅝ'),
                        list('ㅏㅑ'),
                        list('ㅐㅒㅔㅖㅚㅞ'),
                        list('ㅐㅒㅔㅖㅙㅞ'),
                        list('ㅓㅕㅗㅝ'),
                        list('ㅡㅠ'),
                        list('ㅓㅕㅗㅛ'),
                        list('ㅐㅒㅔㅖㅙㅚ'),
                        list('ㅢㅣ'),
                        list('ㅜㅡ'),
                        list('ㅜㅠ'),
                        list('ㅟㅣ'),
                        list('ㅟㅢ')
                        ],
                       [list('ㅎ'),
                        list('ㄲㄳㄺㅋ'),
                        list('ㄱㄳㄺㅋ'),
                        list('ㄱㄲㄺㅋ'),
                        list('ㄵㄶ'),
                        list('ㄴㄶ'),
                        list('ㄴㄵ'),
                        list('ㅅㅆㅈㅊㅌ'),
                        list('ㄽㄾㅀ'),
                        list('ㄱㄲㄳㅋ'),
                        list('ㅁ'),
                        list('ㄿㅂㅄㅍ'),
                        list('ㄹㄾㅀ'),
                        list('ㄹㄽㅀ'),
                        list('ㄼㅂㅄㅍ'),
                        list('ㄹㄽㄾ'),
                        list('ㄻ'),
                        list('ㄼㄿㅄㅍ'),
                        list('ㄼㄿㅂㅍ'),
                        list('ㄷㅆㅈㅊㅌ'),
                        list('ㄷㅅㅈㅊㅌ'),
                        list('ㅇ'),
                        list('ㄷㅅㅆㅊㅌ'),
                        list('ㄷㅅㅆㅈㅌ'),
                        list('ㄱㄲㄳㄺ'),
                        list('ㄷㅅㅆㅈㅊ'),
                        list('ㄼㄿㅂㅄ'),
                        list(chr(12644))
                        ]
                       ]

        self.tpad = self.gtoi.get('<tp>')
        self.spad = self.gtoi.get('<sp>')
        self.sos = self.gtoi.get('<sos>')
        self.eos = self.gtoi.get('<eos>')

    def g2g(self, g, p=0.0, p_gg=0.0):
        if g == chr(9601):
            g = ' '

        if g not in [chr(i) for i in range(32, 127)] and random.random() < p:
            g = self.itog[random.randrange(4, len(self.itog))]

        if g in ' .\n' and random.random() < p_gg:
            g = ''

        return self.bunhe.get(g, g)

    def i2i(self, seq, p=0.0):
        cha_seq = ""
        for n, c in enumerate(seq):
            r = random.random()
            if r < p:
                c = random.choice(self.phocjz[n][self.cjz[n].index(c)])
            elif r > 1 - p / 10:
                c = self.itog[random.randrange(4, len(self.itog))]

            cha_seq += self.bunhe.get(c, c)
        return cha_seq

    def dochi(self, seq, p):
        pp = p
        for i in range(len(seq)):
            if i in [0, len(seq) - 1]:
                continue
            r = random.random()
            if r < pp:
                m = seq[i]
                seq[i] = seq[i + 1]
                seq[i + 1] = m
                pp /= 10
            elif r > 1 - pp:
                m = seq[i]
                seq[i] = seq[i - 1]
                seq[i - 1] = m
                pp /= 10
            else:
                pp = p
        return seq

    def insdel(self, seq, p):
        pp = p
        i = 0
        while i < len(seq):
            r = random.random()
            if r < pp:
                if r < pp/10:
                    seq = seq[:i] + [self.gtoi[random.choice([chr(i) for i in range(32, 127)] + ['\n'])]] + seq[i:]
                else:
                    seq = seq[:i] + [seq[i]] + seq[i:]
                i += 2
                pp /= 10
            elif r > 1 - pp / 2:
                seq = seq[:i] + seq[i+1:]
                pp /= 10
            else:
                i += 1
                pp = p
        return seq

    def flat_seq(self, sen, p_dochi=0.0, p_insdel=0.0, p_mlm=0.0, p_gg=0.0, p_hy=0.0, p_yamin=0.0):
        p_mlm *= random.random()
        p_dochi *= random.random()
        p_hy *= random.random()
        p_yamin *= random.random()
        p_insdel *= random.random()
        p_gg *= random.random()

        sen_seq = []
        for tok in sen:
            sen_seq.extend(self.tok2seq(tok, False, p_mlm, p_gg, p_hy, p_yamin))

        ##################################################################################
        # Dochi
        sen_seq = self.dochi(sen_seq, p_dochi)

        # Insert & Delete
        sen_seq = self.insdel(sen_seq, p_insdel)
        ##################################################################################

        sen_seq = [self.sos] + sen_seq + [self.eos]
        sen_seq = sen_seq[:self.seq_len * 5]
        padding = [self.spad] * (self.seq_len * 5 - len(sen_seq))
        sen_seq.extend(padding)

        return sen_seq

    def sen2seq(self, sen):
        sen_seq = [self.sos] * self.tok_len
        for tok in sen:
            sen_seq.extend(self.tok2seq(tok, True))

        sen_seq.extend([self.eos] * self.tok_len)
        sen_seq = sen_seq[:self.seq_len * self.tok_len]
        padding = [self.spad] * (self.seq_len * self.tok_len - len(sen_seq))
        sen_seq.extend(padding)

        return sen_seq

    def tok2seq(self, tok, do_pad, p_mlm=0.0, p_gg=0.0, p_hy=0.0, p_yamin=0.0):
        tok_seq = []
        if tok == '<n>':
            tok = '\n'
        for cha in tok:
            cha = self.johapsiba.get(cha, cha)
            ##################################################################################
            # YAMIN
            if random.random() < p_yamin:
                cha = self.yamin.get(cha, cha)
            ##################################################################################
            tok_seq.extend(self.cha2seq(cha, p_mlm, p_gg))

        ##################################################################################
        # hanyoung
        if random.random() < p_hy:
            tok_seq = [self.hanyoung.get(j, j) for j in tok_seq]
        ##################################################################################

        tok_seq = [self.gtoi[j] for j in tok_seq]

        if do_pad:
            tok_seq = tok_seq[:self.tok_len]
            padding = [self.tpad] * (self.tok_len - len(tok_seq))
            tok_seq.extend(padding)

        return tok_seq

    def cha2seq(self, cha, p_mlm=0.0, p_gg=0.0):
        ordcha = ord(cha)
        if 44032 <= ordcha <= 55203:
            ordcha -= 44032
            cha3 = [ordcha // (21 * 28),
                    ordcha % (21 * 28) // 28,
                    ordcha % (21 * 28) % 28]

            cha_g = ""
            for i in range(3):
                g = self.cjz[i][cha3[i]]
                if g != chr(12644):
                    cha_g += g

            ##################################################################################
            # Korean MLM
            cha_g = self.i2i(list(cha_g), p_mlm)
            ##################################################################################

            cha_seq = ""
            for i in cha_g:
                cha_seq += self.bunhe.get(i, i)
        else:
            ##################################################################################
            # tuksugiho MLM
            cha_seq = self.g2g(cha, p_mlm, p_gg)
            ##################################################################################

        return list(cha_seq)

    def seq2cha(self, seq):
        cjz = [self.cjz[i].index(seq[i]) for i in range(3)]
        cha = chr(cjz[0] * (21 * 28) + cjz[1] * 28 + cjz[2] + 44032)

        return cha

    def seq2sen(self, grp_seq: list):
        seq = []
        for i in grp_seq:
            seq.append(self.itog[i])

        seq.append('')
        sen = ""
        que = []
        quen = ""
        while len(seq) > 0 or len(que) > 0:

            try:
                if seq[0] in self.cjz[0]:
                    quen += '0'
                elif seq[0] in self.cjz[1]:
                    quen += '1'
                elif seq[0] in ['<tp>', '<sp>', '<sos>']:
                    seq = seq[1:]
                    continue
                else:
                    if seq[0] == '<eos>':
                        seq = ['']
                    ##################################################################################
                    elif seq[0] == '\n':
                        seq[0] = '<n>'
                    ##################################################################################
                    quen += '2'

                que.append(seq[0])
                seq = seq[1:]

            except:
                pass

            banbok = True
            n_bb = 0
            while banbok:
                n_bb += 1
                if n_bb > 20:
                    print('seq', seq)
                    print('que', que)
                    print('qun', quen)
                    print('sen', sen)
                    assert False
                if quen == "11" or quen == "000" or quen == "002":
                    abc = que[0] + que[1]
                    abc = self.hebun.get(abc, abc)
                    sen += abc[0]
                    que = que[3 - len(abc):]
                    quen = quen[3 - len(abc):]
                elif quen == "001" or quen == "10" or quen == "2" or quen == "02" or quen == "12":
                    sen += que[0]
                    que = que[1:]
                    quen = quen[1:]
                elif quen == "011":
                    abc = que[1] + que[2]
                    if abc not in self.hebun:
                        cha3 = [que[0], que[1], chr(12644)]
                        sen += self.seq2cha(cha3)
                        que = que[2:]
                        quen = quen[2:]
                elif quen == "0111" or quen == "01101":
                    abc = que[1] + que[2]
                    cha3 = [que[0], self.hebun.get(abc), chr(12644)]
                    sen += self.seq2cha(cha3)
                    que = que[3:]
                    quen = quen[3:]
                elif quen == "0110" or quen == "0112":
                    abc = que[1] + que[2]
                    if que[3] not in self.cjz[2]:
                        cha3 = [que[0], self.hebun.get(abc), chr(12644)]
                        sen += self.seq2cha(cha3)
                        que = que[3:]
                        quen = quen[3:]
                elif quen == "01100" or quen == "01102":
                    abc = que[1] + que[2]
                    bcd = que[3] + que[4]
                    if bcd not in self.hebun:
                        cha3 = [que[0], self.hebun.get(abc), que[3]]
                        sen += self.seq2cha(cha3)
                        que = que[4:]
                        quen = quen[4:]
                elif quen == "011000" or quen == "011002":
                    abc = que[1] + que[2]
                    bcd = que[3] + que[4]
                    cha3 = [que[0], self.hebun.get(abc), self.hebun.get(bcd)]
                    sen += self.seq2cha(cha3)
                    que = que[5:]
                    quen = quen[5:]
                elif quen == "011001":
                    abc = que[1] + que[2]
                    cha3 = [que[0], self.hebun.get(abc), que[3]]
                    sen += self.seq2cha(cha3)
                    que = que[4:]
                    quen = quen[4:]
                elif quen == "010":
                    if que[2] not in self.cjz[2]:
                        cha3 = [que[0], que[1], chr(12644)]
                        sen += self.seq2cha(cha3)
                        que = que[2:]
                        quen = quen[2:]
                elif quen == "0100" or quen == "0102":
                    bcd = que[2] + que[3]
                    if bcd not in self.hebun:
                        cha3 = [que[0], que[1], que[2]]
                        sen += self.seq2cha(cha3)
                        que = que[3:]
                        quen = quen[3:]
                elif quen == "0101" or quen == "012":
                    cha3 = [que[0], que[1], chr(12644)]
                    sen += self.seq2cha(cha3)
                    que = que[2:]
                    quen = quen[2:]
                elif quen == "01000" or quen == "01002":
                    bcd = que[2] + que[3]
                    cha3 = [que[0], que[1], self.hebun.get(bcd)]
                    sen += self.seq2cha(cha3)
                    que = que[4:]
                    quen = quen[4:]
                elif quen == "01001":
                    cha3 = [que[0], que[1], que[2]]
                    sen += self.seq2cha(cha3)
                    que = que[3:]
                    quen = quen[3:]

                if not quen.endswith('2'):
                    banbok = False

        return sen


if __name__ == "__main__":

    '''
    S2S Test zone
    '''

    itog = ['<tp>', '<sp>', '<sos>', '<eos>'] + \
           [chr(i) for i in range(32, 127)] + \
           list("ㅃㅂㅉㅈㄸㄷㄲㄱㅆㅅㅛㅕㅑㅒㅐㅖㅔㅁㄴㅇㄹㅎㅗㅓㅏㅣㅋㅌㅊㅍㅠㅜㅡ\n")

    change = Change(itog, seq_len=64, tok_len=14)

    text = "나라말이 중국과 달라 한문 한자와 서로 통하지 아니하므로 이런 까닭으로 어리석은 백성들이 말하고자 하는 바가 있어도 끝내 제 뜻을 펴지 못하는 사람이 많다. 내가 이를 불쌍히 여겨 새로 스물 여덟 글자를 만드니 사람마다 하여금 쉽게 익혀 날마다 씀에 편하게 하고자 할 따름이다. 동해물과 백두산이 마르고 닳도록 하느님이 보우하사 우리나라 만세 무궁화 삼천리 화려강산 대한사람 대한으로 길이 보전하세 남산 위에 저 소나무 철갑을 두른 듯 바람 서리 불변함은 우리 기상일세 가을 하늘 공활한데 높고 구름 없이 밝은 달은 우리 가슴 일편단심일세 이 기상과 이 맘으로 충성을 다하여 괴로우나 즐거우나 나라 사랑하세."
    # text = "야얗이 나븐\n 놂아!"

    sp = spm.SentencePieceProcessor(model_file='train44.model')
    text = re.sub('[^ -~ㄱ-ㅣ가-힣\n]', '', text)
    test = sp.encode(re.sub('\n', '<n>', text), out_type=str)

    enc = sp.encode(text)
    '''
    print(test)
    print(sp.decode(enc))
    '''

    while True:
        fake = change.flat_seq(text, p_dochi=.01, p_insdel=.02, p_mlm=.04, p_hy=.1, p_yamin=0.2)
        flat = change.flat_seq(text)
        seq = change.sen2seq(test)
        '''
        #################################################
        fakk = []
        for i in fake:
            if i not in [0, 1, 2, 3]:
                try:
                    fakk.append(change.itog[i])
                except:
                    print("i", i)
                    assert False
        print("fake", "".join(fakk))

        flatt = []
        for i in flat:
            if i not in [0, 1, 2, 3]:
                flatt.append(change.itog[i])

        print("flat", "".join(flatt))

        seqq = []
        for i in seq:
            if i not in [0, 1, 2, 3]:
                seqq.append(change.itog[i])

        print("seq", "".join(seqq))
        ##################################################
        '''
        fake = torch.tensor(fake)
        flat = torch.tensor(flat)
        seq = torch.tensor(seq)

        print(change.seq2sen(fake.tolist()), '\n')
        print(change.seq2sen(flat.tolist()), '\n')
        print(change.seq2sen(seq.tolist()).lstrip(), '\n')
        a = input()
