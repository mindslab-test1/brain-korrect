import sys
import time
import grpc
from concurrent import futures
import subprocess
import argparse
import os
from Action import itest
from math import gcd
import torch

subprocess.run('python -m grpc.tools.protoc -I. --python_out . --grpc_python_out . ./proto/correct.proto', shell=True)
sys.path.append(os.curdir + '/' + 'proto')
from correct_pb2 import Correct
from correct_pb2_grpc import BertCorrectServicer, add_BertCorrectServicer_to_server


class BertCorrectServer(BertCorrectServicer):
    def __init__(self, args):
        self.itest = itest(args=args)

    def GetSentenceSimple(self, request, context):
        return Correct(correct=self.itest.itest(request.sen)[0][1])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--device", default=0, help="device")
    parser.add_argument("--port", default=35015)

    parser.add_argument("-lm", "--load_model_path", default='./models/albert11_10_094.pt', type=str, help="load model path")

    parser.add_argument("-ah", "--attention_hierarchy", default=11, type=str, help="Attention unit hierarchy (13)")
    parser.add_argument("-sl", "--seq_len", default=128, type=int, help="Max length of sequence (number of characters) (128)")
    parser.add_argument("-at", "--attention_type", default='full', type=str, help="'lsh' or 'full' ('full')")
    parser.add_argument("-hs", "--hidden_size", default=384, type=int, help="Hidden size of transformer model (256)")
    parser.add_argument("-nl", "--n_layers", default=12, type=int, help="Number of layers (12)")
    parser.add_argument("-mh", "--multi_heads", default=12, type=int, help="Number of attention multi-heads (12)")

    args = parser.parse_args()

    assert torch.cuda.is_available(), "You must be able to use GPU(CUDA)!"

    assert args.attention_type in ['lsh', 'full'], \
        f"-at ({args.attention_type}) is not our attention. Please choose 'lsh' or 'full'..."

    args.attention_hierarchy = str(args.attention_hierarchy)

    if len(args.attention_hierarchy) > 3:
        ans = input(f"\nYou input -ah {args.attention_hierarchy} "
                    f"({len(args.attention_hierarchy)}).\n"
                    f"Training may be heavy & slow...")

    lcm = 1
    for i in args.attention_hierarchy:
        lcm = lcm * int(i) // gcd(lcm, int(i))

    args.seq_len = ((args.seq_len * 3 - 1) // lcm + 1) * lcm

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    bert_correct_server = BertCorrectServer(args=args)
    bert_intent_servicer = add_BertCorrectServicer_to_server(bert_correct_server, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    server.start()

    print("BERT Correct grpc server starting at 0.0.0.0:{}".format(args.port))

    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)
