import sentencepiece as spm
from Change import Change
from collections import Counter
import matplotlib.pyplot as plt
import tqdm

# spm.SentencePieceTrainer.train(input='pret_trn_news_wiki9M.txt', model_prefix='train1', vocab_size=80000, user_defined_symbols=['<n>'], input_sentence_size=90000000, shuffle_input_sentence=True, max_sentencepiece_length=1, bos_id=2, eos_id=3, pad_id=1, character_coverage=1.0)

itog = ['<unk>', '<pad>', '<sos>', '<eos>'] + \
       [chr(i) for i in range(32, 127)] + \
       [chr(9601)] + \
       [chr(i) for i in range(12593, 12645)]

change = Change(itog, seq_len=32, tok_len=20)

sp = spm.SentencePieceProcessor(model_file="trainreal.model")

c = Counter()

for i in tqdm.tqdm(range(sp.vocab_size())):
    t = change.tok2seq(sp.id_to_piece(i), do_pad=False)
    if 1 <= len(t) <= 1:
        print(t)
    c[len(t)] += 1

print(c)

# plt.bar(c.keys(), c.values())
plt.bar([i for i in range(max(c.keys())+1)], [sum([c[i] for i in range(j+1)]) for j in range(max(c.keys())+1)])
plt.show()
