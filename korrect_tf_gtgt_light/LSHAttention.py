import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Function
from Attention import Attention
import math


def sort_key_val(t1, t2, dim=-1):
    values, indices = t1.sort(dim=dim)
    t2 = t2.expand_as(t1)
    return values, t2.gather(dim, indices)


def batched_index_select(values, indices):
    last_dim = values.shape[-1]
    return values.gather(dim=3, index=indices.unsqueeze(-1).expand(-1, -1, -1, -1, last_dim))


def look_one_back(x):
    x_extra = torch.cat([x[:, :, :, -1:, ...], x[:, :, :, :-1, ...]], dim=3)
    return torch.cat([x, x_extra], dim=4)


def chunked_sum(tensor, chunks=1):

    *orig_size, last_dim = tensor.shape
    tensor = tensor.reshape(-1, last_dim)
    summed_tensors = [c.sum(dim=-1) for c in tensor.chunk(chunks, dim=0)]
    out = torch.cat(summed_tensors, dim=0).reshape(orig_size)
    return out


class LSHAttention(nn.Module):
    def __init__(self, n_round, d_bucket, dec=False, dropout=0.1):
        super().__init__()
        self.dec = dec
        self.n_round = n_round
        self.d_bucket = d_bucket
        self.dropout = nn.Dropout(dropout)
        self.n_bucket = None

    def lsh(self, inp):
        # 입력된 놈의 모양은? : [batch_size, n_head, seq_len, d_head]

        *bh, seq_len, d_head = inp.size()

        # 랜덤 매트릭스를 만들어줍시다. 멀티 라운드 횟수만큼 만들고, 원하는 바구니 갯수의 반 만큼만 해줍니다. 나중에 마이너스 붙여서 2배로 불릴꺼에요.
        rand_matrix = torch.randn(*bh, d_head, self.n_round, self.n_bucket // 2, device=inp.device)

        # 시퀀스의 각 토큰이 각 라운드마다, 각 바구니에 들어갈 확률을 랜덤 매트릭스를 통해 설정해줍니다.
        rotated = torch.einsum('...ij,...jkl->...kil', inp, rand_matrix)

        # 마이너스를 붙여서 범위를 2배로 늘리면, 원래 원했던 바구니 갯수만큼 생기겠죠? argmax 를 해서 어느 바구니에 들어갈 확률이 젤 높은지 구해줍니다.
        hash = torch.argmax(torch.cat([rotated, -rotated], dim=-1), dim=-1)

        # 아래를 계산하면, 각 라운드의 바구니번호가 겹치지 않게 돼요.
        hash = hash * self.n_round + torch.arange(self.n_round, device=inp.device).reshape(1, 1, -1, 1).expand_as(hash)
        # hash : 미니배치 안의 각 헤드마다, 각 시퀀스의 각 토큰들이 어느 바구니에 들어갈 지에 대한 정보가 담겨 있어요.
        # hash 의 모양은? : [batch_size, n_head, n_round, seq_len]
        hash = hash.reshape(*bh, self.n_round, seq_len)
        # [batch_size, n_head, n_round * seq_len]
        return hash

    def forward(self, qk, k, v, mask=None):
        # qk, v : [batch_size, n_head, seq_len, d_head]
        *bh, seq_len, d_head = qk.size()

        assert seq_len % (
                self.d_bucket * 2) == 0, f'seq_len ({seq_len}) must be divisible by d_bucket * 2 ({self.d_bucket * 2})'

        self.n_bucket = seq_len // self.d_bucket
        # bucket : [batch_size, n_head, n_round, seq_len]
        bucket = self.lsh(qk)
        # ticker : [0, 1, 2, ..., seq_len]
        ticker = torch.arange(seq_len, device=qk.device)[None, None, None, :].expand_as(bucket)
        # ticker = torch.arange(seq_len, device=qk.device)
        # 바구니에 원래 시퀀스에서의 index 정보까지 새겨넣어주마!
        bucket_t = seq_len * bucket + ticker

        bucket_t = bucket_t.detach()

        # 바구니를 해쉬에 따라 정렬, 원래 위치를 sticker에 기억하자
        sbucket_t, sticker = bucket_t.sort(dim=-1)
        _, undo_sort = sticker.sort(dim=-1)
        del ticker
        # 저희한테 grad 계산하지 마세요!
        sbucket_t = sbucket_t.detach()
        sticker = sticker.detach()
        undo_sort = undo_sort.detach()

        # qk 와 v 의 첫 계산 : sbucket_t 에 따라 각 라운드별 각 위치로!
        sqk = batched_index_select(qk.unsqueeze(2).expand(-1, -1, self.n_round, -1, -1), sticker)
        sv = batched_index_select(v.unsqueeze(2).expand(-1, -1, self.n_round, -1, -1), sticker)

        # 청크로 쪼개자.
        n_chunk = self.n_round * self.n_bucket
        bq_t = bkv_t = torch.reshape(sticker, (*bh, self.n_round, self.n_bucket, -1))
        bqk = torch.reshape(sqk, (*bh, self.n_round, self.n_bucket, -1, d_head))
        bv = torch.reshape(sv, (*bh, self.n_round, self.n_bucket, -1, d_head))

        bq = bqk
        bk = F.normalize(bqk, p=2, dim=-1).type(bq.type())

        # 자기랑, 자기보다 하나 앞에 있는 청크, 총 2개의 청크를 어탠션 대상으로 한다.
        # 청크 (1) (2) (3) (4) -> (4,1) (1,2) (2,3) (3,4)
        # (4,1) : 1 의 앞에 4가 나오는건 아니지만, 그냥 뭐 그렇다고 칩시다~
        bk = look_one_back(bk)
        bv = look_one_back(bv)
        bkv_t = look_one_back(bkv_t)

        # 첫 dot-product 어탠션 계산
        dots = torch.einsum('...ie,...je->...ij', bq, bk) * (d_head ** -0.5)
        masked_value = torch.finfo(dots.dtype).min

        # 제로패딩이 된 분들에게는 해쉬 정렬되더라도 끝까지 쫓아가서 마스킹할겁니다.
        # mask : [batch_size, n_head, seq_len] : pad_idx 가 False 로 되어있음.
        if mask is not None:
            mask = mask.squeeze(1).expand(*bh, -1)
            mq = mask.unsqueeze(2).expand(-1, -1, self.n_round, -1).gather(3, sticker).reshape((*bh, self.n_round, self.n_bucket, -1))
            mkv = look_one_back(mq)
            this_mask = mq[:, :, :, :, :, None] * mkv[:, :, :, :, None, :]
            dots.masked_fill_(~this_mask, masked_value)
            del this_mask

        # 자기 보다 값이 큰 놈 (자기보다 뒤(미래)에 나올 놈) 은 어탠션 안줄꺼야!
        # BERT 셀프어탠션에 쓰고 싶으니까 주석으로 해놓음.
        if self.dec:
            this_mask = bq_t[:, :, :, :, :, None] < bkv_t[:, :, :, :, None, :]
            dots.masked_fill_(this_mask, masked_value)
            del this_mask

        # 자기자신의 값에는 어탠션 주지마....
        # 근데 어탠션 줄 곳이 마땅치 않으면 쪼금(-5e4)은 줘도 돼...
        self_mask = bq_t[:, :, :, :, :, None] == bkv_t[:, :, :, :, None, :]
        dots.masked_fill_(self_mask, -5e4)
        del self_mask

        # 드디어 기다리고 기다리던, 같은 바구니에 있는 토큰 끼리만 어탠션을 구하는 부분!
        bq_buckets = bkv_buckets = torch.reshape(sbucket_t // seq_len, (*bh, self.n_round, self.n_bucket, -1))
        bkv_buckets = look_one_back(bkv_buckets)
        bucket_mask = bq_buckets[:, :, :, :, :, None] != bkv_buckets[:, :, :, :, None, :]
        dots.masked_fill_(bucket_mask, masked_value)
        del bucket_mask

        # qk 페어 중복 방지 (공부하자)
        locs1 = undo_sort // bq_t.shape[-1]
        locs2 = (locs1 + 1) % n_chunk
        locs1 = bucket * n_chunk + locs1
        locs2 = bucket * n_chunk + locs2
        del bucket
        locs = torch.cat([
            torch.reshape(locs1, (*bh, self.n_round, seq_len)),
            torch.reshape(locs2, (*bh, self.n_round, seq_len)),
        ], 2).permute((0, 1, 3, 2))

        # locs : [bsz, n_head, seq_len, n_round * 2]
        # sticker : [bsz, n_head, n_round, seq_len]

        slocs = batched_index_select(locs.unsqueeze(2).expand(-1, -1, self.n_round, -1, -1), sticker)
        # slocs : [bsz, n_head, n_round, seq_len, n_round * 2]

        b_locs = torch.reshape(slocs, (*bh, self.n_round, self.n_bucket, -1, 2 * self.n_round))
        # b_locs = [bsz, n_head, n_round, n_bucket, d_bucket, 2 * n_round]

        b_locs1 = b_locs[:, :, :, :, :, None, :self.n_round]
        # b_locs1 = [bsz, n_head, n_round, n_bucket, d_bucket, 1, n_round]

        bq_locs = b_locs1.expand(b_locs.shape[:5] + (2, self.n_round))
        # bq_locs = [bsz, n_head, n_round, n_bucket, d_bucket, 2, n_round]

        bq_locs = torch.reshape(bq_locs, b_locs.shape)
        # bq_locs = [bsz, n_head, n_round, n_bucket, d_bucket, 2 * n_round]
        bkv_locs = look_one_back(b_locs)
        # bkv_locs = [bsz, n_head, n_round, n_bucket, d_bucket * 2, 2 * n_round]

        dup_counts = (bq_locs[:, :, :, :, :, None, :] == bkv_locs[:, :, :, :, None, :, :])
        # dup_counts : [bsz, n_head, n_round, n_bucket, d_bucket, d_bucket * 2, 2 * n_round]
        # for memory considerations, chunk summation of last dimension for counting duplicates

        dup_counts = chunked_sum(dup_counts, chunks=bh[0]*bh[1]*self.n_round)

        dup_counts = dup_counts.detach()
        assert dup_counts.shape == dots.shape
        dots = dots - torch.log(dup_counts + 1e-9)
        del dup_counts

        # 어탠션 소프트맥스 하는 부분
        dots_logsumexp = torch.logsumexp(dots, dim=-1, keepdim=True)
        dots = torch.exp(dots - dots_logsumexp).type(dots.type())
        dropped_dots = self.dropout(dots)
        bo = torch.einsum('...ij,...je->...ie', dropped_dots, bv)

        so = torch.reshape(bo, (*bh, self.n_round, -1, d_head))

        slogits = torch.reshape(dots_logsumexp, (*bh, self.n_round, -1,))

        # 역정렬 (순서 원래대로 돌려놓기)
        class UnsortLogits(Function):
            @staticmethod
            def forward(ctx, so, slogits):
                so = so.detach()
                slogits = slogits.detach()
                o = batched_index_select(so, undo_sort)
                _, logits = sort_key_val(sticker, slogits, dim=-1)
                return o, logits

            @staticmethod
            def backward(ctx, grad_x, grad_y):
                so_grad = batched_index_select(grad_x, sticker)
                slogits_grad = grad_y.gather(dim=-1, index=sticker)
                return so_grad, slogits_grad

        o, logits = UnsortLogits.apply(so, slogits)
        o = torch.reshape(o, (*bh, self.n_round, seq_len, d_head))
        logits = torch.reshape(logits, (*bh, self.n_round, seq_len, 1))
        probs = torch.exp(logits - torch.logsumexp(logits, dim=2, keepdim=True))

        out = torch.sum(o * probs, dim=2)
        '''
        공부하자
        attn = torch.empty(0, device=qk.device)        
        attn_unsort = ((bq_t * seq_len)[:, :, :, None] + bkv_t[:, :, None, :])
        attn_unsort = attn_unsort.view(batch_size * total_hashes, -1).long()
        unsorted_dots = torch.zeros(batch_size * total_hashes, seq_len * seq_len, device=device)
        unsorted_dots.scatter_add_(1, attn_unsort, dots.view_as(attn_unsort))
        del attn_unsort
        unsorted_dots = unsorted_dots.reshape(batch_size, total_hashes, seq_len, seq_len)
        attn = torch.sum(unsorted_dots[:, :, 0:query_len, :] * probs, dim=1)
        '''
        return out


if __name__ == "__main__":

    bsz = 1
    n_head = 3
    seq_len = 4096
    d_head = 128

    a = torch.randn(bsz, n_head, seq_len, d_head, device='cuda:0')
    fula = Attention(scaled=d_head ** 0.5)
    # print("LSH : ", T() - nnow)

    lsha = LSHAttention(n_round=2, d_bucket=int(2**(math.log2(seq_len)//2)), dec=True)

    b = lsha(a, None, a)



