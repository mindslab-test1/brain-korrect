import torch
from torch.utils.data import IterableDataset
from Change import Change
import sentencepiece as spm
import re
import random


class Dataset(IterableDataset):
    def __init__(self, input_src, p, seq_len, tok_len, change, sp):
        self.change = change
        self.input_data = input_src
        self.seq_len = seq_len
        self.tok_len = tok_len
        self.sp = sp

        ##################################################################################
        # [p_dochi, p_insdel, p_mlm, p_gg, p_hy, p_yamin]
        p_all = [.05, .1, .2, .5, .5, 1.0]
        ##################################################################################

        self.pp = [i * p for i in p_all]
        self.idx = [i for i in range(len(p_all))]

        self.idxx = [
            1,
            2,
            3, 3,
            4, 4, 4, 4, 4,
            5, 5, 5, 5, 5, 5,
            6, 6, 6
        ]

    def __iter__(self):
        for inp in self.input_data:
            reid = random.sample(self.idx, random.choice(self.idxx))
            pp = [0.0 if i in reid else j * len(reid) / len(self.idx) for i, j in enumerate(self.pp)]

            inp = re.sub('[^ -~ㄱ-ㅣ가-힣\n]', '', inp)
            inputs = self.change.flat_seq(inp, p_dochi=pp[0], p_insdel=pp[1], p_mlm=pp[2], p_gg=pp[3], p_hy=pp[4], p_yamin=pp[5])
            self.change.seq_len += 1

            inp = re.sub('\n', '<n>', inp)
            decode = self.change.sen2seq(self.sp.encode(inp, out_type=str))
            self.change.seq_len -= 1

            target = self.sp.encode(inp) + [self.sp.eos_id()]
            target = target[:self.seq_len]
            padding = [self.sp.pad_id()] * (self.seq_len - len(target))
            target.extend(padding)

            yield {'enc': torch.tensor(inputs),
                   'dec': torch.tensor(decode[:-self.tok_len]),
                   'tar': torch.tensor(decode[self.tok_len:]),
                   'tok': torch.tensor(target)}


if __name__ == "__main__":
    itog = ['<tp>', '<sp>', '<sos>', '<eos>'] + \
           [chr(i) for i in range(32, 127)] + \
           list("ㅃㅂㅉㅈㄸㄷㄲㄱㅆㅅㅛㅕㅑㅒㅐㅖㅔㅁㄴㅇㄹㅎㅗㅓㅏㅣㅋㅌㅊㅍㅠㅜㅡ\n")

    change = Change(itog=itog, seq_len=32, tok_len=14)
    sp = spm.SentencePieceProcessor(model_file='train44.model')

    inp = ["안녕하세요 마인즈랩입니다.\n하하"]
    d = Dataset(inp, 1, 32, 14, change, sp)
    while True:
        for i in d.__iter__():
            '''
            print(change.seq2sen(i['enc']))
            print(change.seq2sen(i['dec']))
            print(change.seq2sen(i['tar']))
            '''
            print(i['enc'].size())
            print(i['dec'].size())
            print(i['tar'].size())
            print(i['tok'].size())
            a = input()
