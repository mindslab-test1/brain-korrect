import torch.nn as nn
import torch
from Attention import Attention
from LSHAttention import LSHAttention
import math


class MultiheadFull(nn.Module):
    def __init__(self, n_head, d_q, d_kv, d_head, is_training=True):
        super().__init__()

        self.n_head = n_head
        self.w_q = nn.Linear(d_q, n_head * d_head)
        self.w_k = nn.Linear(d_kv, n_head * d_head)
        self.w_v = nn.Linear(d_kv, n_head * d_head)
        self.f_c = nn.Linear(n_head * d_head, d_q)

        self.attention = Attention(scaled=d_head ** 0.5, is_training=is_training)

        self.layer_norm1 = nn.LayerNorm(d_q, eps=1e-6)
        self.layer_norm2 = nn.LayerNorm(d_q, eps=1e-6)

    def forward(self, q, k, v, dropout=0.1, pad_mask=None, sub_mask=False, self_mask=False):
        # q : [batch_size, seq_len, d_model]
        bsz = q.size(0)
        q_l = q.size(1)
        kv_l = k.size(1)

        res = q
        q = self.layer_norm1(q)

        q = self.w_q(q)
        k = self.w_k(k)
        v = self.w_v(v)

        q = q.reshape(bsz, q_l, self.n_head, -1).transpose(1, 2)
        k = k.reshape(bsz, kv_l, self.n_head, -1).transpose(1, 2)
        v = v.reshape(bsz, kv_l, self.n_head, -1).transpose(1, 2)

        # q = self.attention(q, k, v, pad_mask=pad_mask, sub_mask=sub_mask, self_mask=q_l == kv_l)
        q = self.attention(q, k, v, dropout=dropout, pad_mask=pad_mask, sub_mask=sub_mask, self_mask=self_mask)
        q = q.transpose(1, 2).contiguous().view(bsz, q_l, -1)

        q = self.layer_norm2(self.f_c(q))
        q += res

        return q


class Multihead(nn.Module):
    def __init__(self, n_head, d_model, d_head, seq_len, tok_len, n_round, d_bucket, attention_type):
        super().__init__()

        self.n_head = n_head
        self.d_head = d_head
        self.w_q = nn.Linear(d_model, n_head * d_head)
        self.w_k = nn.Linear(d_model, n_head * d_head)
        self.w_v = nn.Linear(d_model, n_head * d_head)
        self.f_c = nn.Linear(n_head * d_head, d_model)
        self.ek = None
        if seq_len is not None:
            error = .1
            self.ek = nn.Parameter(torch.randn(seq_len, int(math.log(d_head) // (error**2))), requires_grad=True)

        self.attention_type = attention_type
        if attention_type == 'lsh':
            self.attention = LSHAttention(n_round=n_round, d_bucket=d_bucket, dec=False)
        else:
            self.attention = Attention(scaled=d_head ** 0.5)

        self.layer_norm1 = nn.LayerNorm(d_model, eps=1e-6)
        self.layer_norm2 = nn.LayerNorm(d_model, eps=1e-6)

    def sliceview(self, q):
        bsz, sl = q.size(0), q.size(1)
        q = q.reshape(bsz, sl, self.n_head, -1).transpose(1, 2)
        return q

    def forward(self, q, k, v, mask=None):
        # q : [batch_size, seq_len, d_model]
        bsz, sl = q.size(0), q.size(1)

        res = q
        q = self.layer_norm1(q)

        q = self.w_q(q)
        k = self.w_k(k) if self.attention_type == 'full' else None
        v = self.w_v(v)

        # q_l : [n_chunk, bsz, n_head, sl, d_head]
        q, v = self.sliceview(q), self.sliceview(v)
        k = self.sliceview(k) if self.attention_type == 'full' else k

        if mask is None:
            mask = [None] * len(self.hierarchy)

        for i in range(len(self.hierarchy)):
            if self.ek is not None:
                k[i] = torch.matmul(k[i].transpose(2, 3), self.ek).transpose(2, 3)
                v[i] = torch.matmul(v[i].transpose(2, 3), self.ek).transpose(2, 3)
            q[i] = self.attention(q[i], k[i], v[i], mask=mask[i])
            q[i] = q[i].transpose(1, 2).contiguous().view(bsz, sl, -1)

        q = torch.cat(q, dim=-1)
        q = self.layer_norm2(self.f_c(q))
        q += res

        return q


if __name__ == "__main__":
    test = torch.randn(4, 200, 6, 3, 768)
    mask = [torch.randint(0, 2, (4, 1, 3600)), torch.randint(0, 2, (4, 1, 1200)), torch.randint(0, 2, (4, 1, 200))]
    multihead = Multihead(12, 768, 64, 2, 8, True)
    output = multihead(test, test, test, mask)
    # print(len(output))
