import argparse
from Openfile import openfile
import os
import torch.multiprocessing as mp
import torch
from Action import train, itest
import re


def mpddp(gpu, gpus, args):
    input_file = openfile(src=args.input, seq_len=args.seq_len, gpu=gpu, gpus=gpus)
    train(gpu, gpus, args, input_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", default=None, type=str, help="input")
    parser.add_argument("-sp", "--model_save_path", default=None, type=str, help="model save path")
    parser.add_argument("-lm", "--load_model_path", default=None, type=str, help="load model path")

    parser.add_argument("-v", "--vocab", default='data/train44.model', type=str, help="vocab model path")

    parser.add_argument("-bsr", "--batch_size_train", default=8, type=int, help="batch size of train")
    parser.add_argument("-bse", "--batch_size_test", default=1, type=int, help="batch size of val")

    parser.add_argument("-sl", "--seq_len", default=128, type=int, help="max length of sequence")
    parser.add_argument("-tl", "--tok_len", default=14, type=int, help="max length of tok")

    parser.add_argument("-hs", "--hidden_size", default=64, type=int, help="hidden size of transformer model")
    parser.add_argument("-enl", "--enc_n_layers", default=12, type=int, help="number of encoder layers")
    parser.add_argument("-dnl", "--dec_n_layers", default=12, type=int, help="number of decoder layers")
    parser.add_argument("-mh", "--multi_heads", default=12, type=int, help="number of attention heads")

    parser.add_argument("-lr", "--learning_rate", default=1e-3, type=float, help="learning rate of optimizer")

    parser.add_argument("-ipe", "--iter_per_epoch", default=None, type=int, help="number of iteration")
    parser.add_argument("-es", "--epoch_start", default=0, type=int, help="epoch start point")
    parser.add_argument("-ee", "--epoch_end", default=20, type=int, help="epoch end point")

    parser.add_argument("-rl", "--reset_loss", action='store_true', help="reset aver_loss when load saved model_file")
    parser.add_argument("-ft", "--fine_tune", action='store_true', help="load only encoder not whole transformer")
    parser.add_argument("-if", "--inference", action='store_true', help="do inference?")
    parser.add_argument("-pol", "--print_output_len", default=64, type=int, help="length of output to print")
    parser.add_argument("-lf", "--log_frequency", default=10, type=int, help="log per iter")
    parser.add_argument("-pp", "--print_parameters", action='store_true', help="print or not")

    args = parser.parse_args()

    assert torch.cuda.is_available(), "You must be able to use GPU(CUDA)!"

    if not args.inference:
        assert args.input is not None, "This is Train mode! You must --input(-i) .txt or .json file to train!"
        if args.model_save_path is None:
            print("There is NO SAVE path of model! But You can train this model with No save...^^")

    if not args.inference:
        os.environ['MASTER_ADDR'] = 'localhost'
        os.environ['MASTER_PORT'] = '8888'
        gpus = torch.cuda.device_count()
        print(f"Use {gpus} devices!")
        mp.spawn(mpddp, nprocs=gpus, args=(gpus, args))

    else:
        itt = itest(args)
        while True:
            input_text = input("input : ")
            if input_text in ["end", "끝"]:
                print("See you next time~")
                break
            output_text = itt.itest(input_text)[0][1]
            output_text = re.sub('<n>', '\n', output_text)

            print("Korrect :", output_text)

