import os
import sys
import grpc
import argparse
sys.path.append(os.curdir+'/'+'proto')
from correct_pb2 import Sentence
from correct_pb2_grpc import BertCorrectStub
import re


class CorrectClient:
    def __init__(self, ip="localhost", port=35015):
        self.server_ip = ip
        self.server_port = port

        self.stub = BertCorrectStub(
            grpc.insecure_channel(self.server_ip + ":" + str(self.server_port))
        )

    def get_corrects(self, sen):
        sentence = Sentence()
        sentence.sen = sen

        correct_sen = self.stub.GetSentenceSimple(sentence)
        return correct_sen.correct


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--ip", default='localhost', type=str, help="IP address")
    parser.add_argument("--port", default=35015, type=int, help="port number")

    args = parser.parse_args()

    print(f"Connecting to {args.ip}:{args.port}...")
    correct_client = CorrectClient(ip=args.ip, port=args.port)
    print("Welcome to BERT Correct gRPC Server!")
    print("If you want to end, input 'end' or '끝'...")
    while True:
        input_text = input("input : ")
        if input_text in ["end", "끝"]:
            print("See you next time~")
            break
        # print("Sentence :", input_text)
        output_text = correct_client.get_corrects(input_text + '\n')
        output_text = re.sub('<sot>', '', output_text)
        output_text = re.sub('<eot>', '', output_text)
        output_text = re.sub('<cls>', '\n', output_text)
        print(" Correct :", output_text)


