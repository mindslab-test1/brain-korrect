import torch.nn as nn
import torch.nn.functional as F


class Feedforward(nn.Module):
    def __init__(self, d_model, d_hidden, dropout=0.1):
        super().__init__()

        self.w_1 = nn.Linear(d_model, d_hidden)
        self.w_2 = nn.Linear(d_hidden, d_model)

        self.dropout = nn.Dropout(dropout)
        self.layer_norm1 = nn.LayerNorm(d_model, eps=1e-6)
        self.layer_norm2 = nn.LayerNorm(d_model, eps=1e-6)

    def forward(self, x):
        res = x
        x = self.layer_norm1(x)
        x = self.w_2(F.gelu(self.w_1(x)))
        x = self.layer_norm2(x)
        x += res
        return x
