import tqdm
import random
import sys


def openfile(src, seq_len, gpu, gpus):
    print("Input file ReadingJung...")
    with open(src, 'r', encoding='utf-8') as t:
        tt = t.readlines()
    print("Reading Complete!")
    input_data = []

    ttt = ""
    if gpu == 0:
        tt = tqdm.tqdm(tt)

    for text in tt:
        if len(text) > 1:
            if len(ttt + text) < seq_len and random.random() < 0.3:
                ttt += text
            else:
                if len(ttt) > 1:
                    input_data.append(ttt)
                ttt = text
        else:
            if len(ttt) > 1:
                input_data.append(ttt)
            ttt = ""

    return input_data[len(input_data) // gpus * gpu:len(input_data) // gpus * (gpu + 1)]


if __name__ == "__main__":
    a = openfile('pret_dev_news_wiki9M.txt', 200)
    print(sys.getsizeof(a))
