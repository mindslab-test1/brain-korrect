import random

import sentencepiece as spm
import torch
import torch.distributed as dist
import torch.nn as nn
import tqdm
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data import DataLoader

from Change import Change
from IterableDataset import Dataset as ID
from Lamb import Lamb
from Tranformer import Transformer

itog = ['<tp>', '<sp>', '<sos>', '<eos>'] + \
       [chr(i) for i in range(32, 127)] + \
       list("ㅃㅂㅉㅈㄸㄷㄲㄱㅆㅅㅛㅕㅑㅒㅐㅖㅔㅁㄴㅇㄹㅎㅗㅓㅏㅣㅋㅌㅊㅍㅠㅜㅡ\n")


def train(gpu, gpus, args, input_file):
    print(f"Splitting data on {gpu}...")
    ttr = args.batch_size_train + args.batch_size_test
    ttr = len(input_file) // ttr * args.batch_size_train

    trds = input_file[:ttr]
    teds = input_file[ttr:]
    del input_file

    print(f"Setting manual_seed on {gpu}...")
    torch.manual_seed(0)
    torch.cuda.manual_seed_all(0)

    print(f"Initiating process {gpu}...")
    dist.init_process_group(backend='nccl',
                            init_method='env://',
                            world_size=gpus,
                            rank=gpu)

    iteration = 0
    train_aver_loss = [10.0] * 1000
    test_aver_loss = [10.0] * 1000
    jeongdap_chr, munzesu_chr = 0, 0
    jeongdap_chrtok, munzesu_chrtok = 0, 0
    jeongdap_tok, munzesu_tok = 0, 0

    print(f"Making Changer on {gpu}...")
    change = Change(itog=itog, seq_len=args.seq_len, tok_len=args.tok_len)

    sp = spm.SentencePieceProcessor(model_file=args.vocab)

    print(f"Setting Device {gpu}...")
    torch.cuda.set_device(gpu)

    print(f"Making Transformer on {gpu}...")
    model = Transformer(n_vocab=len(itog), n_sp_vocab=sp.vocab_size(), d_word_vec=args.hidden_size // 6,
                        pad_idx=itog.index('<sp>'),
                        d_model=args.hidden_size,
                        d_hidden=args.hidden_size * 4,
                        enc_n_layer=args.enc_n_layers,
                        dec_n_layer=args.dec_n_layers,
                        n_head=args.multi_heads,
                        d_head=args.hidden_size // args.multi_heads,
                        seq_len=args.seq_len, tok_len=args.tok_len)

    if gpu == 0:
        learning_info = f'data_path : {args.input}\n' + \
                        f'model_save_path : {args.model_save_path}\n' + \
                        f'load_model_path : {args.load_model_path}\n' + \
                        f'vocab : {args.vocab}\n' + \
                        f'vocab_size : {sp.vocab_size()}\n' + \
                        f'batch_size_train : {args.batch_size_train}\n' + \
                        f'batch_size_test : {args.batch_size_test}\n' + \
                        f'train_mode : Train\n' + \
                        f'seq_len : {args.seq_len}\n' + \
                        f'tok_len : {args.tok_len}\n' + \
                        f'hidden_size : {args.hidden_size}\n' + \
                        f'enc_n_layers : {args.enc_n_layers}\n' + \
                        f'dec_n_layers : {args.dec_n_layers}\n' + \
                        f'multi_heads : {args.multi_heads}\n' + \
                        f'epoch_start : {args.epoch_start}\n' + \
                        f'epoch_end : {args.epoch_end}\n' + \
                        f'iter_per_epoch : {args.iter_per_epoch}\n' + \
                        f'learning_rate : {args.learning_rate}\n' + \
                        f'Total Number of Parameters : {sum(p.nelement() for p in model.parameters())}\n'

        if args.model_save_path is not None:
            with open(f"{args.model_save_path}.txt", 'w', encoding='utf-8') as info:
                info.write(learning_info)

        print("\n", learning_info, "\n")

        if args.print_parameters:
            for name, p in model.named_parameters():
                print(name, p.nelement())

    print(f"Moving model to {gpu}...")
    model = model.cuda(gpu)

    optim = Lamb(model.parameters(), lr=args.learning_rate, betas=(0.9, 0.999), weight_decay=0.01)

    print(f"Loading model to {gpu}...")
    if args.load_model_path is not None:
        load_model = torch.load(args.load_model_path, map_location=f"cuda:{gpu}")

        if args.fine_tune:
            model.encoder.load_state_dict(load_model['model_state_dict'], strict=False)
        else:
            model.load_state_dict(load_model['lm_state_dict'], strict=False)
            try:
                optim.load_state_dict(load_model['optimizer_state_dict'])
            except:
                pass

            if not args.reset_loss:
                iteration = load_model['iteration']
                train_aver_loss = load_model['train_loss']
                test_aver_loss = load_model['test_loss']
        del load_model
    else:
        with open(f"{args.model_save_path}-train_log.txt", 'w', encoding='utf-8') as f:
            f.write("")

    print(f"Wrapping model with DDP on {gpu}...")
    model = DDP(model, device_ids=[gpu], find_unused_parameters=True)

    criterion = nn.CrossEntropyLoss(ignore_index=itog.index('<sp>'))
    criterion_tok = nn.CrossEntropyLoss(ignore_index=sp.pad_id())

    for epoch in range(args.epoch_start, args.epoch_end + 1):
        print(f"Shuffling data on {gpu}...")
        random.shuffle(trds)
        random.shuffle(teds)

        print(f"Making Dataset on {gpu}...")
        if args.iter_per_epoch is not None:
            dataset = ID(input_src=trds[:args.iter_per_epoch * args.batch_size_train], change=change,
                         seq_len=args.seq_len, tok_len=args.tok_len, sp=sp, p=epoch / args.epoch_end)
            testset = ID(input_src=teds[:args.iter_per_epoch * args.batch_size_test], change=change,
                         seq_len=args.seq_len, tok_len=args.tok_len, sp=sp, p=epoch / args.epoch_end)
        else:
            dataset = ID(input_src=trds, change=change,
                         seq_len=args.seq_len, tok_len=args.tok_len, sp=sp, p=epoch / args.epoch_end)
            testset = ID(input_src=teds, change=change,
                         seq_len=args.seq_len, tok_len=args.tok_len, sp=sp, p=epoch / args.epoch_end)

        print(f"Making DataLoader on {gpu}...")
        dataloader = DataLoader(dataset=dataset, batch_size=args.batch_size_train, shuffle=False,
                                num_workers=0, pin_memory=True, drop_last=True)
        testloader = DataLoader(dataset=testset, batch_size=args.batch_size_test, shuffle=False,
                                num_workers=0, pin_memory=True, drop_last=True)

        print(f"Training Start! on {gpu}...")
        if gpu == 0:
            datatqdm = tqdm.tqdm(zip(dataloader, testloader), total=args.iter_per_epoch)
        else:
            datatqdm = zip(dataloader, testloader)
        del dataloader, testloader
        for train_data, test_data in datatqdm:
            ###########################################################################################################
            train_data = {k: v.cuda(gpu) for k, v in train_data.items()}

            train_output, train_tok_out = model.forward(train_data['enc'], train_data['dec'], dropout=0.1 if iteration < 200000 else 0.0)
            train_loss = criterion(train_output.permute(0, 2, 1), train_data['tar']) + criterion_tok(
                train_tok_out.permute(0, 2, 1), train_data['tok'])

            train_aver_loss = train_aver_loss[1:] + [train_loss.item()]
            ###########################################################################################################
            optim.zero_grad()
            train_loss.backward()
            optim.step()
            ###########################################################################################################
            test_data = {k: v.cuda(gpu) for k, v in test_data.items()}

            test_output, test_tok_out = model.forward(test_data['enc'], test_data['dec'])
            test_loss = criterion(test_output.permute(0, 2, 1), test_data['tar']) + criterion_tok(
                test_tok_out.permute(0, 2, 1), test_data['tok'])

            test_aver_loss = test_aver_loss[1:] + [test_loss.item()]
            ###########################################################################################################
            iteration += 1

            if gpu == 0:
                test_target_mask = test_data['tar'] != itog.index('<sp>')
                test_tok_mask = test_data['tok'] != sp.pad_id()

                jeongdap = torch.argmax(test_output, dim=-1) == test_data['tar'].masked_fill(~test_target_mask, -1)
                jeongdap_chr += torch.sum(jeongdap).item()

                jeongdap_chrtok += torch.sum(jeongdap.reshape(-1, args.seq_len, args.tok_len).all(dim=-1)).item()

                jeongdap = torch.argmax(test_tok_out, dim=-1) == test_data['tok'].masked_fill(~test_tok_mask, -1)
                jeongdap_tok += torch.sum(jeongdap).item()

                munzesu_chr += torch.sum(test_target_mask).item()
                munzesu_chrtok += torch.sum(test_target_mask.reshape(-1, args.seq_len, args.tok_len).any(dim=-1)).item()
                munzesu_tok += torch.sum(test_tok_mask).item()

                if iteration % args.log_frequency == 0:
                    input_log = test_data['enc'][0].tolist()
                    input_log = change.seq2sen(input_log)[:args.print_output_len]

                    output_log = torch.argmax(test_output[0], dim=-1).tolist()
                    output_log = change.seq2sen(output_log)[:args.print_output_len]

                    target_log = test_data['tar'][0].tolist()
                    target_log = change.seq2sen(target_log)[:args.print_output_len]

                    tok_log = torch.argmax(test_tok_out[0], dim=-1).tolist()
                    if sp.eos_id() in tok_log:
                        tok_log = tok_log[:tok_log.index(sp.eos_id())]
                    tok_log = sp.decode(tok_log)[:args.print_output_len]

                    log = (
                        f"epoch : {epoch:2d}, "
                        f"i : {iteration:6d}\n"
                        f"train_aver_loss : {sum(train_aver_loss) / 1000:2.6f}, "
                        f"test_aver_loss : {sum(test_aver_loss) / 1000:2.6f}, "
                        f"train_loss : {train_loss:2.6f}, "
                        f"test_loss : {test_loss:2.6f}\n"
                        f"accuracy_chr : {jeongdap_chr}/{munzesu_chr} ({jeongdap_chr / munzesu_chr * 100:2.3f}%), "
                        f"accuracy_chrtok : {jeongdap_chrtok}/{munzesu_chrtok} ({jeongdap_chrtok / munzesu_chrtok * 100:2.3f}%), "
                        f"accuracy_tok : {jeongdap_tok}/{munzesu_tok} ({jeongdap_tok / munzesu_tok * 100:2.3f}%)\n"
                        f" input : {input_log}\n"
                        f"output : {output_log.lstrip()}\n"
                        f"target : {target_log.lstrip()}\n"
                        f" token : {tok_log.strip()}\n")

                    datatqdm.write(log)

                    if iteration % (args.log_frequency * 50) == 0:
                        log = (
                            f"epoch : {epoch:2d}, "
                            f"i : {iteration:6d}, "
                            f"train_aver_loss : {sum(train_aver_loss) / 1000:2.6f}, "
                            f"test_aver_loss : {sum(test_aver_loss) / 1000:2.6f}\n")
                        with open(f"{args.model_save_path}-train_log.txt", 'a', encoding='utf-8') as f:
                            f.write(log)

                        if args.model_save_path is not None:
                            saving = {
                                'epoch': epoch,
                                'iteration': iteration,
                                'train_loss': train_aver_loss,
                                'test_loss': test_aver_loss,
                                'lm_state_dict': model.module.state_dict(),
                                'optimizer_state_dict': optim.state_dict(),
                                'model_state_dict': model.module.encoder.state_dict()
                            }

                            torch.save(saving, args.model_save_path +
                                       f"_{epoch:02d}_{iteration // (args.log_frequency * 500):03d}.pt")
                            datatqdm.write(f"Model saved!")

                    jeongdap_chr, munzesu_chr = 0, 0
                    jeongdap_chrtok, munzesu_chrtok = 0, 0
                    jeongdap_tok, munzesu_tok = 0, 0

    dist.destroy_process_group()


class itest:
    def __init__(self, args):

        print(f"Making Changer...")
        self.change = Change(itog=itog, seq_len=args.seq_len, tok_len=args.tok_len)

        self.sp = spm.SentencePieceProcessor(model_file=args.vocab)

        print(f"Making Transformer...")
        self.model = Transformer(n_vocab=len(itog), n_sp_vocab=self.sp.vocab_size(), d_word_vec=args.hidden_size // 6, pad_idx=itog.index('<sp>'),
                                 d_model=args.hidden_size,
                                 d_hidden=args.hidden_size * 4,
                                 enc_n_layer=args.enc_n_layers,
                                 dec_n_layer=args.dec_n_layers,
                                 n_head=args.multi_heads,
                                 d_head=args.hidden_size // args.multi_heads,
                                 seq_len=args.seq_len, tok_len=args.tok_len, is_training=False)

        print(f"Moving model...")
        self.model = self.model.cuda()

        print(f"Loading model...")
        load_model = torch.load(args.load_model_path, map_location="cuda:0")
        self.model.load_state_dict(load_model['lm_state_dict'], strict=False)
        del load_model

        self.seq_len = args.seq_len
        self.tok_len = args.tok_len

    def itest(self, input_file):
        testset = ID(input_src=[input_file], change=self.change,
                     seq_len=self.seq_len, tok_len=self.tok_len, sp=self.sp, p=0.0)
        testloader = DataLoader(dataset=testset, batch_size=1, shuffle=False, num_workers=0, pin_memory=True)

        allout = []
        for test_data in testloader:
            test_data = {k: v.cuda() for k, v in test_data.items()}
            with torch.no_grad():
                test_output = self.model.forward(test_data['enc'], test_data['dec'], do_test=True)

            for inpu, outp in zip(test_data['enc'], test_output):
                out = [self.change.seq2sen(inpu.tolist()),
                       self.change.seq2sen(torch.argmax(outp, dim=-1).tolist())]
                allout.append(out)

        return allout
