import torch.nn as nn
import numpy as np
import torch


def get_position_angle_vec(position, d_model):
    return [position / np.power(10000, 2 * (j // 2) / d_model) for j in range(d_model)]


def get_sinusoid_encoding_table(seq_len, d_hidden):
    sinusoid_table = np.array([get_position_angle_vec(i, d_hidden) for i in range(seq_len)])
    sinusoid_table[:, 0::2] = np.sin(sinusoid_table[:, 0::2])
    sinusoid_table[:, 1::2] = np.cos(sinusoid_table[:, 1::2])
    return torch.FloatTensor(sinusoid_table).unsqueeze(0)


class Position(nn.Module):
    def __init__(self, d_model, seq_len=200):
        super().__init__()
        self.register_buffer('pos_table', get_sinusoid_encoding_table(seq_len, d_model))

    def forward(self, x):
        return self.pos_table[:, :x.size(1)].clone().detach()
