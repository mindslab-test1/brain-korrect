import torch.nn as nn
from .Position import Position
from .Token import Token


class Embedding(nn.Module):
    def __init__(self, num_target_vocab, d_word_vec, pad_idx, d_model, seq_len):
        super().__init__()
        self.token = Token(num_target_vocab, d_word_vec, d_model, pad_idx)
        self.position = Position(d_model, seq_len)

    def forward(self, x):
        x = self.position(x) + self.token(x)
        return x
