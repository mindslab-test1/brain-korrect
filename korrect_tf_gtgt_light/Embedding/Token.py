import torch.nn as nn


class Token(nn.Module):
    def __init__(self, num_target_vocab, d_word_vec, d_model, pad_idx):
        super().__init__()
        self.embed = nn.Embedding(num_target_vocab, d_word_vec, padding_idx=pad_idx)
        self.linear = nn.Linear(d_word_vec, d_model)

    def forward(self, x):
        embed = self.embed(x)
        embed = self.linear(embed)
        return embed
