import torch


def get_pad_mask(seq: torch.Tensor, pad_idx, tl):
    bsz = seq.size(0)
    mask = seq == pad_idx
    mask = mask.reshape(bsz, 1, 1, -1, tl).all(dim=-1)
    return mask


if __name__ == "__main__":
    a = torch.randint(0, 2, [4, 12, 3])
    b = get_pad_mask(a, 0, "134")

    for i in b:
        print(i.size())
