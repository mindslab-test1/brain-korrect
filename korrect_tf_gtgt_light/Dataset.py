import torch
from torch.utils.data import Dataset


class Dataset(Dataset):
    def __init__(self, input_src, epoch, change):

        self.change = change
        self.input_data = input_src

        print(f"{len(self.input_data)} sentences loaded!")

        self.epoch = epoch

    def __len__(self):
        return len(self.input_data)

    def __getitem__(self, item):
        input_seq, target_change = self.change.tok2seq(self.input_data[item], r=0.3, t=True, epoch=self.epoch)
        target_all, _ = self.change.tok2seq(self.input_data[item], r=0.0, t=False)

        return {'input_seq': torch.tensor(input_seq),
                'target_change': torch.tensor(target_change),
                'target_all': torch.tensor(target_all)}
