import torch
import torch.nn as nn
from Embedding.Embedding import Embedding
from Multihead import MultiheadFull
from Feedforward import Feedforward
from Utility import get_pad_mask


class Encoder(nn.Module):
    def __init__(
            self, n_vocab, d_model, d_word_vec, d_hidden, n_layer, n_head,
            d_head, pad_idx, seq_len, tok_len, dropout=0.1, is_training=True):
        super().__init__()

        self.embedding = Embedding(n_vocab, d_word_vec, pad_idx, d_model, seq_len * tok_len)
        self.self_attention = MultiheadFull(n_head=n_head, d_q=d_model, d_kv=d_model, d_head=d_head, is_training=is_training)

        self.feedforward = nn.ModuleList([Feedforward(d_model, d_hidden, dropout=dropout) for _ in range(n_layer)])

        self.layer_norm1 = nn.LayerNorm(d_model, eps=1e-6)
        self.layer_norm2 = nn.LayerNorm(d_model, eps=1e-6)

    def forward(self, seq, mask, dropout=0.1):
        seq = self.layer_norm1(self.embedding(seq))

        for ff in self.feedforward:
            seq = self.self_attention(seq, seq, seq, dropout=dropout, pad_mask=mask, self_mask=True)
            seq = ff(seq)

        seq = self.layer_norm2(seq)
        return seq


class Decoder(nn.Module):
    def __init__(
            self, n_vocab, d_model, d_word_vec, d_hidden, n_layer, n_head,
            d_head, pad_idx, seq_len, tok_len, dropout=0.1, is_training=True):
        super().__init__()

        self.seq_len = seq_len
        self.tok_len = tok_len

        self.embedding = Embedding(n_vocab, d_word_vec, pad_idx, d_model // tok_len, seq_len * tok_len)
        self.self_attention = MultiheadFull(n_head=n_head, d_q=d_model, d_kv=d_model, d_head=d_head, is_training=is_training)
        self.ende_attention = nn.ModuleList([MultiheadFull(n_head=n_head, d_q=d_model, d_kv=d_model // tok_len, d_head=d_head, is_training=is_training) for _ in range(n_layer)])
        self.feedforward = nn.ModuleList([Feedforward(d_model, d_hidden, dropout=dropout) for _ in range(n_layer)])

        self.layer_norm1 = nn.LayerNorm(d_model, eps=1e-6)
        self.layer_norm2 = nn.LayerNorm(d_model // tok_len, eps=1e-6)

    def forward(self, enc_seq, dec_seq, enc_mask, dec_mask, dropout=0.1):
        dec_seq = self.embedding(dec_seq)

        dec_seq = dec_seq.reshape(dec_seq.size(0), self.seq_len, -1)

        dec_seq = self.layer_norm1(dec_seq)

        for ed, ff in zip(self.ende_attention, self.feedforward):
            dec_seq = self.self_attention(dec_seq, dec_seq, dec_seq, dropout=dropout, pad_mask=dec_mask, sub_mask=True)
            dec_seq = ed(dec_seq, enc_seq, enc_seq, dropout=dropout, pad_mask=enc_mask)
            dec_seq = ff(dec_seq)

        dec_seq = dec_seq.reshape(dec_seq.size(0), self.seq_len * self.tok_len, -1)

        dec_seq = self.layer_norm2(dec_seq)
        return dec_seq


class Transformer(nn.Module):
    def __init__(self,
                 n_vocab, n_sp_vocab, d_word_vec, pad_idx,
                 d_model, d_hidden, enc_n_layer, dec_n_layer, n_head, d_head, seq_len, tok_len,
                 dropout=0.1, is_training=True):

        super().__init__()

        self.pad_idx = pad_idx
        self.tok_len = tok_len
        self.seq_len = seq_len
        self.encoder = Encoder(n_vocab=n_vocab, d_word_vec=d_word_vec, d_model=d_model, d_hidden=d_hidden,
                               n_layer=enc_n_layer, n_head=n_head, d_head=d_head, pad_idx=pad_idx,
                               seq_len=seq_len, tok_len=tok_len, dropout=dropout, is_training=is_training)
        self.decoder = Decoder(n_vocab=n_vocab, d_word_vec=d_word_vec, d_model=d_model * tok_len, d_hidden=d_hidden * tok_len,
                               n_layer=dec_n_layer, n_head=n_head, d_head=d_head * tok_len, pad_idx=pad_idx,
                               seq_len=seq_len, tok_len=tok_len, dropout=dropout, is_training=is_training)

        self.unlinear = nn.Linear(d_model, d_word_vec)
        self.unembed = nn.Linear(d_word_vec, n_vocab)

        self.unl_tok = nn.Linear(d_model * tok_len, d_word_vec * tok_len)
        self.une_tok = nn.Linear(d_word_vec * tok_len, n_sp_vocab)

        for p in self.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)

        self.decoder.embedding.token.embed.weight.data = self.encoder.embedding.token.embed.weight.data
        self.decoder.embedding.token.linear.weight.data = self.encoder.embedding.token.linear.weight.data

        self.unlinear.weight.data = self.encoder.embedding.token.linear.weight.data.t()
        self.unembed.weight.data = self.encoder.embedding.token.embed.weight.data

    def forward(self, enc_seq, dec_seq, do_test=False, dropout=0.1):
        enc_mask = get_pad_mask(enc_seq, self.pad_idx, 1)
        dec_mask = get_pad_mask(dec_seq, self.pad_idx, self.tok_len)
        enc_seq = self.encoder(enc_seq, enc_mask, dropout=dropout)
        v = 1
        while True:
            dec_out = self.decoder(enc_seq, dec_seq, enc_mask, dec_mask, dropout=dropout)
            dec_seq_out = self.unlinear(dec_out)
            dec_seq_out = self.unembed(dec_seq_out)

            dec_out = dec_out.reshape(dec_out.size(0), self.seq_len, -1)
            dec_tok_out = self.unl_tok(dec_out)
            dec_tok_out = self.une_tok(dec_tok_out)
            if v < dec_seq.size(1) and do_test:
                dec_seq_out = torch.argmax(dec_seq_out, dim=-1)
                dec_seq = torch.cat([dec_seq[:, :1], dec_seq_out[:, :-1]], dim=1)
                v += 1
            else:
                break

        return dec_seq_out, dec_tok_out


if __name__ == "__main__":
    test = torch.randint(0, 120, (4, 200, 6, 3))
    src_mask = get_pad_mask(test, 0, "13")
    print([i.size() for i in src_mask])
